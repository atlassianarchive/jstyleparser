# Atlassian's fork of jStyleParser has moved

Please do future work in Atlassian's fork of the project on github: https://github.com/atlassian/jStyleParser

This repo here is a fork from before the original project moved to github and maven.